﻿using System;

using Xamarin.Forms;

namespace InStalker
{
	public class TabPage : TabbedPage
	{
		public TabPage ()
		{
			this.Children.Add (new Follows());
			this.Children.Add (new Search ());
		}
	}
}


