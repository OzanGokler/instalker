﻿using System;

using Xamarin.Forms;

namespace InStalker
{
	public class LogIn : ContentPage
	{
		public LogIn ()
		{
			Button btnLogin = new Button
			{
				Text = "LOGIN",
				WidthRequest = 150,
				HeightRequest = 50,
				BackgroundColor = Color.Aqua,
				TextColor = Color.Red
			};

			btnLogin.Clicked += BtnLogin_Clicked;

			Content = new StackLayout { 
				Children = {
					new Label { Text = "Hello Stakler" },
					btnLogin
				}
			};
		}

		void BtnLogin_Clicked (object sender, EventArgs e)
		{
			Navigation.PushModalAsync(new TabPage());		
		}
	}
}


